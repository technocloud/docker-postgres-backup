# docker-postgres-backup

This docker image provides functionality:

1. Backup your PostgresSQL database to sql file using pg_dump
2. Compress sql file using LZMA2
3. Upload compressed file to S3 storage
4. Backup running schedule (monthly, weekly, daily, hourly)

# Versions
You can use four versions:
- PostgreSQL 14: `registry.gitlab.com/technocloud/docker-postgres-backup/docker-postgres-backup:pg14`
- PostgreSQL 15: `registry.gitlab.com/technocloud/docker-postgres-backup/docker-postgres-backup:pg15`
- PostgreSQL 16: `registry.gitlab.com/technocloud/docker-postgres-backup/docker-postgres-backup:pg16`

# SETTINGS
You can set some envs vars:
- SCHEDULE - can be monthly, weekly, daily, hourly (by default)
- DB_USER - user to connect DB, default is postgres
- DB_PASSWORD - password to connect DB, default is postgres
- DB_HOST - host to connect DB, default is localhost
- DB_PORT - port to connect DB, default is 5432
- DB_NAME - database to back up, default is postgres
- TIME_ZONE - timezone for datetime using in filenames, default is Europe/Tallinn
- COMPRESSION_LEVEL - level of LZMA compression, default is 7
- PREFIX - prefix for backup filename, default is empty

Also, please define settings for S3 storage:
- AWS_S3_REGION_NAME - default nl-ams
- AWS_S3_ENDPOINT_URL - default `https://s3.nl-ams.scw.cloud`
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_BUCKET_NAME

# Build docker

`docker buildx build --platform=linux/arm64/v8,linux/amd64 --push --tag registry.gitlab.com/technocloud/docker-postgres-backup/docker-postgres-backup:pgXX -f Dockerfile.pgXX .`
